Class {
	#name : #'VW_OpenGLFont',
	#superclass : #Object,
	#category : #'JRMC-porting-support'
}

{ #category : #api }
VW_OpenGLFont >> text: aString position: aPoint [
	| pointer data textureVectors textureCoordinates position vectors coordinates buffer |
	vectors := (Array new: (6 * 4 * aString size)) writeStream.
	coordinates := (Array new: (6 * 2 * aString size)) writeStream.
	position := aPoint.
	aString keysAndValuesDo: [:index :character |
		| rectangle origin advance A B C D S T U V offset step |
		rectangle := rectangles at: (character asInteger - characters first + 1).
		origin := rectangle at: 1.
		advance := rectangle at: 3.
		A := Array with: position x with: ascent + descent + aPoint y with: 0 with: 1.
		B := Array with: position x with: aPoint y with: 0 with: 1.
		C := Array with: position x + advance x with: aPoint y with: 0 with: 1.
		D := Array with: position x + advance x with: ascent + descent + aPoint y with: 0 with: 1.
		offset := origin x / maximum x.
		step := advance x / maximum x + offset.
		S := Array with: offset with: 1.
		T := Array with: offset with: 0.
		U := Array with: step with: 0.
		V := Array with: step with: 1.
		vectors nextPutAll: A; nextPutAll: D; nextPutAll: C; nextPutAll: C; nextPutAll: B; nextPutAll: A.
		coordinates nextPutAll: S; nextPutAll: V; nextPutAll: U; nextPutAll: U; nextPutAll: T; nextPutAll: S.
		position x: position x + advance x].

	pointer := gl GLfloat malloc: 6 * 6 * aString size.
	data := FloatArray withAll: vectors contents, coordinates contents.
	buffer := Buffer newArrayBuffer.
	buffer data: data mode: gl STREAM_DRAW.
	pointer release.
	textureVectors := buffer asAttributeArray: OpenGLVector4.
	textureCoordinates := buffer asAttributeArray: OpenGLVector2 offset: 6 * 4 * aString size.
	^OpenGLRenderState new
		uniformAt: 'texture' put: texture;
		attributeAt: 'position' put: textureVectors;
		attributeAt: 'textureCoordinate' put: textureCoordinates;
		indices: (0 to: 6 * aString size - 1);
		yourself
]
