Class {
	#name : #ReplayFileStrategy,
	#superclass : #Strategy,
	#instVars : [
		'stream'
	],
	#category : #'JRMC-Core'
}

{ #category : #'instance creation' }
ReplayFileStrategy class >> on: filename [

	^self new
		on: filename asFileName;
		yourself
]

{ #category : #'instance creation' }
ReplayFileStrategy >> on: aFilename [

	| fileStream instructionStream |
	fileStream := aFilename readStream.
	instructionStream := WriteStream on: (Array new: 1000).

	fileStream upTo: Character cr.

	[fileStream atEnd] whileFalse: [
		| direction |
		fileStream upTo: $*.
		direction := OpalCompiler evaluate: (fileStream upTo: Character cr) trimSeparators.
		instructionStream nextPut: direction ].
	fileStream close.

	^stream := ReadStream on: instructionStream contents
]

{ #category : #running }
ReplayFileStrategy >> stepRobot: aRobot [

	| direction index |
	stream atEnd ifTrue: [^aRobot direction: nil].
	direction := stream peek.
	(direction isKindOf: Array) ifFalse: [^aRobot direction: stream next].

	aRobot direction: (direction at: (index := aRobot board robot robots indexOf: aRobot)).
	index = aRobot board robot robots size ifTrue: [stream next]
]
