Class {
	#name : #HighValueRegionStrategy,
	#superclass : #Strategy,
	#instVars : [
		'highValueRegions'
	],
	#category : #'JRMC-Core'
}

{ #category : #identification }
HighValueRegionStrategy class >> teamName [

	^'David Buck'
]

{ #category : #running }
HighValueRegionStrategy >> setupOn: aRobot [

	highValueRegions := SortedCollection new.
	self findHighValueRegionsFor: aRobot.
]
