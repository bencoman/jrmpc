Class {
	#name : #RandomWalkStrategy,
	#superclass : #Strategy,
	#instVars : [
		'random'
	],
	#category : #'JRMC-Core'
}

{ #category : #identification }
RandomWalkStrategy class >> teamName [

	^'Competition Organizer'
]

{ #category : #'initialize-release' }
RandomWalkStrategy >> initialize [

	random := Random new
]

{ #category : #accessing }
RandomWalkStrategy >> random [

	^random
]

{ #category : #accessing }
RandomWalkStrategy >> random: anObject [

	random := anObject
]

{ #category : #running }
RandomWalkStrategy >> stepRobot: aRobot [

	aRobot direction: (#(#left #right #up #down) at: (random next * 4) truncated + 1).
]
