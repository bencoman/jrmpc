Class {
	#name : #Strategy,
	#superclass : #Object,
	#category : #'JRMC-Core'
}

{ #category : #'instance creation' }
Strategy class >> new [
	"Answer a newly created and initialized instance."

	^super new initialize
]

{ #category : #identification }
Strategy class >> teamName [

	^'Team name not provided'
]

{ #category : #'initialize-release' }
Strategy >> initialize [
]

{ #category : #running }
Strategy >> setupOn: aRobot [
]

{ #category : #running }
Strategy >> stepRobot: aRobot [

	self subclassResponsibility
]
