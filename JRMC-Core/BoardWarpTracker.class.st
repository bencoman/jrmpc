Class {
	#name : #BoardWarpTracker,
	#superclass : #Object,
	#instVars : [
		'warp1',
		'warp2'
	],
	#category : #'JRMC-Core'
}

{ #category : #events }
BoardWarpTracker >> redButtonPressedEvent: aMouseButtonEvent [ 

	| location |
	warp1 isNil ifTrue: [^super redButtonPressedEvent: aMouseButtonEvent].

	location := controller view hoverLocationFor: aMouseButtonEvent point - controller view parent bounds origin.
	self controller view model at: location put: (warp2 := Array with: #warp with: (warp1 at: 2) with: (warp1 at: 3)).
	warp1 at: 2 put: location x.
	warp1 at: 3 put: location y.
	
	self view invalidate
]

{ #category : #events }
BoardWarpTracker >> redButtonReleasedEvent: aMouseButtonEvent [

	(warp1 notNil and: [warp2 notNil]) ifTrue: [^super redButtonReleasedEvent: aMouseButtonEvent]
]

{ #category : #private }
BoardWarpTracker >> setUpFor: aMouseMovedEvent [

	| location |
	location := controller view hoverLocationFor: aMouseMovedEvent point - controller view parent bounds origin.
	self controller view model at: location put: (warp1 := Array with: #warp with: location x with: location y).
	self view invalidate
]
