Class {
	#name : #BoardJumpTracker,
	#superclass : #Object,
	#instVars : [
		'jumpValue'
	],
	#category : #'JRMC-Core'
}

{ #category : #private }
BoardJumpTracker >> finishSelectionFor: aPoint [

	| value |
	super finishSelectionFor: aPoint.
	value := VW_Dialog request: 'Distance?' initialAnswer: '10'.
	jumpValue at: 2 put: (value asNumber)
]

{ #category : #accessing }
BoardJumpTracker >> jumpValue [

	^jumpValue
]

{ #category : #accessing }
BoardJumpTracker >> jumpValue: anObject [

	jumpValue := anObject
]

{ #category : #private }
BoardJumpTracker >> setUpFor: aMouseMovedEvent [

	| location |
	location := controller view hoverLocationFor: aMouseMovedEvent point - controller view parent bounds origin.
	self controller view model at: location put: (jumpValue := Array with: #jump with: nil).
	self view invalidate
]
