Class {
	#name : #BestNeighborStrategy,
	#superclass : #Strategy,
	#category : #'JRMC-Core'
}

{ #category : #identification }
BestNeighborStrategy class >> teamName [

	^'Competition Organizer'
]

{ #category : #running }
BestNeighborStrategy >> stepRobot: aRobot [

	| bestValue bestDirection thisValue |
	bestValue := 0.
	bestDirection := #left.
	thisValue := aRobot navigator left value.
	(thisValue isInteger and: [thisValue > bestValue]) ifTrue: [
		bestValue := thisValue.
		bestDirection := #left].
	
	thisValue := aRobot navigator right value.
	(thisValue isInteger and: [thisValue > bestValue]) ifTrue: [
		bestValue := thisValue.
		bestDirection := #right].

	thisValue := aRobot navigator up value.
	(thisValue isInteger and: [thisValue > bestValue]) ifTrue: [
		bestValue := thisValue.
		bestDirection := #up].

	thisValue := aRobot navigator down value.
	(thisValue isInteger and: [thisValue > bestValue]) ifTrue: [
		bestValue := thisValue.
		bestDirection := #down].

	aRobot direction: bestDirection
]
