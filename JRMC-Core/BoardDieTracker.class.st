Class {
	#name : #BoardDieTracker,
	#superclass : #Object,
	#category : #'JRMC-Core'
}

{ #category : #private }
BoardDieTracker >> trackSelectionFor: aPoint [

	| location |
	location := controller view hoverLocationFor: aPoint.
	self controller view model at: location put: #(die).
	self view invalidate
]
