Class {
	#name : #BoardHillTracker,
	#superclass : #Object,
	#instVars : [
		'location',
		'targetLocation',
		'value'
	],
	#category : #'JRMC-Core'
}

{ #category : #private }
BoardHillTracker >> finishSelectionFor: aPoint [

	| radius board |
	board := self controller view model.
	radius := (targetLocation - location) r.

	self controller view line: nil.

	board size x negated // 2 to: board size x // 2 do: [:x |
		board size y negated // 2 to: board size y // 2 do: [:y |
			| normal |
			normal := (self normalAt: (location dist: (x @ y + location)) mean: 0.0 radius: radius) * (2.0 * Float pi) sqrt * radius.
			normal > 0.1 ifTrue: [
				board at: location + (x @ y) put: (normal * self controller view value) truncated]]].

	self controller view invalidate.
	super finishSelectionFor: aPoint
]

{ #category : #actions }
BoardHillTracker >> normalAt: x mean: mean radius: standardDeviation [

	^(1.0 / (standardDeviation * (2.0 * Float pi) sqrt) )
		* ((x - mean) squared / (2.0 * standardDeviation squared)) negated exp
]

{ #category : #private }
BoardHillTracker >> setUpFor: aMouseMovedEvent [

	location := controller view hoverLocationFor: aMouseMovedEvent point - controller view parent bounds origin.
	self view invalidate
]

{ #category : #private }
BoardHillTracker >> trackSelectionFor: aPoint [

	targetLocation := controller view hoverLocationFor: aPoint.
	controller view line: (LineSegment
				from: (controller view boxForCellAt: location) center rounded
				to: (controller view boxForCellAt: targetLocation) center rounded).
	self view invalidate
]

{ #category : #accessing }
BoardHillTracker >> value [

	^value
]

{ #category : #accessing }
BoardHillTracker >> value: anObject [

	value := anObject
]
