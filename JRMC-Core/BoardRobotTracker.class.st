Class {
	#name : #BoardRobotTracker,
	#superclass : #Object,
	#category : #'JRMC-Core'
}

{ #category : #private }
BoardRobotTracker >> addRobotAt: location [

	| robot |
	robot := self controller view model robot.
	robot isNil
		ifTrue: [self controller view model placeRobot: (Robot new location: location; yourself)]
		ifFalse: [
			(robot isKindOf: Robot)
				ifTrue: [self controller model
						placeRobot: (CompositeRobot new
							addRobot: robot;
							yourself)].
				self controller model robot addRobot: (Robot new location: location; yourself)].
]

{ #category : #private }
BoardRobotTracker >> removeRobotAt: location [

	| robot |
	robot := self controller view model robot.
	robot isNil ifTrue: [^self].

	(robot isKindOf: Robot) ifTrue: [^self controller model robot: nil].
	
	robot removeRobot: (robot robotAtLocation: location).
	robot robots size = 1 ifTrue: [
		self controller model placeRobot: robot robots first]
]

{ #category : #private }
BoardRobotTracker >> setUpFor: aMouseMovedEvent [

	| location |
	location := controller view hoverLocationFor: aMouseMovedEvent point - controller view parent bounds origin.

	(self controller view model hasRobotAt: location)
		ifTrue: [self removeRobotAt: location]
		ifFalse: [self addRobotAt: location].

	self view invalidate
]
