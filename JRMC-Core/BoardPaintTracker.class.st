Class {
	#name : #BoardPaintTracker,
	#superclass : #Object,
	#category : #'JRMC-Core'
}

{ #category : #private }
BoardPaintTracker >> trackSelectionFor: aPoint [

	| location |
	location := controller view hoverLocationFor: aPoint.
	self controller view model at: location put: self controller view paintValueHolder value.
	self view invalidate
]
