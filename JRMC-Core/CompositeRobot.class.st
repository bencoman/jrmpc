Class {
	#name : #CompositeRobot,
	#superclass : #Object,
	#instVars : [
		'robots'
	],
	#category : #'JRMC-Core'
}

{ #category : #'instance creation' }
CompositeRobot class >> new [
	"Answer a newly created and initialized instance."

	^super new initialize
]

{ #category : #adding }
CompositeRobot >> addRobot: aRobot [

	robots add: aRobot
]

{ #category : #accessing }
CompositeRobot >> board: anObject [

	robots do: [:robot | robot board: anObject]
]

{ #category : #running }
CompositeRobot >> claimCell [

	robots do: #claimCell
]

{ #category : #accessing }
CompositeRobot >> direction [

	^(robots collect: #direction) asArray
]

{ #category : #accessing }
CompositeRobot >> direction: anArray [

	^robots with: anArray do: [:robot :direction | robot direction: direction]
]

{ #category : #'initialize-release' }
CompositeRobot >> initialize [

	robots := OrderedCollection new
]

{ #category : #testing }
CompositeRobot >> isAt: aPoint [

	^robots anySatisfy: [:robot | robot isAt: aPoint]
]

{ #category : #testing }
CompositeRobot >> isDead [

	^robots allSatisfy: #isDead
]

{ #category : #testing }
CompositeRobot >> isSetup [

	^robots allSatisfy: #isSetup
]

{ #category : #running }
CompositeRobot >> move [

	robots do: #move
]

{ #category : #running }
CompositeRobot >> move: anArray [

	robots with: anArray do: [:robot :direction |  robot move: direction]
]

{ #category : #copying }
CompositeRobot >> postCopy [

	robots := robots collect: #copy
]

{ #category : #adding }
CompositeRobot >> removeRobot: aRobot [

	robots remove: aRobot
]

{ #category : #adding }
CompositeRobot >> robotAtLocation: aLocation [

	^robots detect: [:each | each location = aLocation]
]

{ #category : #accessing }
CompositeRobot >> robots [

	^robots
]

{ #category : #accessing }
CompositeRobot >> robots: anObject [

	robots := anObject
]

{ #category : #accessing }
CompositeRobot >> score [

	^robots inject: 0 into: [:total :robot | total + robot score]
]

{ #category : #running }
CompositeRobot >> setup [

	robots do: #setup
]

{ #category : #running }
CompositeRobot >> step [

	robots do: #step
]

{ #category : #printing }
CompositeRobot >> storeOn: aStream [

	aStream
		nextPut: $(;
		nextPutAll: self class name;
		nextPutAll: ' new robots: ';
		nextPutAll: robots storeString;
		nextPutAll: '; yourself)'
]

{ #category : #accessing }
CompositeRobot >> strategy [

	^robots first strategy
]

{ #category : #accessing }
CompositeRobot >> strategy: aStrategy [

	robots do: [:each | each strategy: aStrategy]
]

{ #category : #accessing }
CompositeRobot >> strategyClass [

	^robots first strategyClass
]

{ #category : #accessing }
CompositeRobot >> strategyClass: aClass [

	robots do: [:each | each strategyClass: aClass]
]
