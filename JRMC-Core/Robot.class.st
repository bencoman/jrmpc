Class {
	#name : #Robot,
	#superclass : #Object,
	#instVars : [
		'location',
		'score',
		'board',
		'direction',
		'strategyClass',
		'strategy',
		'isSetup',
		'navigatorClass',
		'isDead'
	],
	#category : #'JRMC-Core'
}

{ #category : #'instance creation' }
Robot class >> new [

	^super new initialize
]

{ #category : #accessing }
Robot >> board [

	^board
]

{ #category : #accessing }
Robot >> board: anObject [

	board := anObject.
	board at: self location put: 0
]

{ #category : #moving }
Robot >> claimCell [

	score := score + (self board claimCell: location).
	self hasDied ifTrue: [self markDead].
]

{ #category : #accessing }
Robot >> direction [

	^direction
]

{ #category : #accessing }
Robot >> direction: anObject [

	direction := anObject
]

{ #category : #running }
Robot >> hasDied [

	^score < 0 or: [(board at: location) = #(die)]
]

{ #category : #'initialize-release' }
Robot >> initialize [

	location := 0@0.
	score := 0.
	isSetup := false.
	isDead := false
]

{ #category : #testing }
Robot >> isAt: aPoint [

	^location = aPoint
]

{ #category : #testing }
Robot >> isDead [

	^isDead
]

{ #category : #accessing }
Robot >> isSetup [

	^isSetup
]

{ #category : #accessing }
Robot >> location [

	^location
]

{ #category : #accessing }
Robot >> location: anObject [

	location := anObject
]

{ #category : #accessing }
Robot >> markDead [

	isDead := true
]

{ #category : #moving }
Robot >> move [

	self isDead ifTrue: [^self].
	self move: self direction
]

{ #category : #moving }
Robot >> move: aSymbol [

	aSymbol = #left ifTrue: [^self moveLeft].
	aSymbol = #right ifTrue: [^self moveRight].
	aSymbol = #up ifTrue: [^self moveUp].
	aSymbol = #down ifTrue: [^self moveDown].
	aSymbol = #stop ifTrue: [^self board markFinished].
	^nil
]

{ #category : #moving }
Robot >> moveDown [

	location := self navigator down location.
	self claimCell
]

{ #category : #moving }
Robot >> moveLeft [

	location := self navigator left location.
	self claimCell
]

{ #category : #moving }
Robot >> moveRight [

	location := self navigator right location.
	self claimCell
]

{ #category : #moving }
Robot >> moveUp [

	location := self navigator up location.
	self claimCell
]

{ #category : #running }
Robot >> navigator [

	^self navigatorClass new
		location: location;
		board: board;
		yourself
]

{ #category : #accessing }
Robot >> navigatorClass [

	^navigatorClass ifNil: [BoardNavigator]
]

{ #category : #accessing }
Robot >> navigatorClass: aClass [

	navigatorClass := aClass
]

{ #category : #accessing }
Robot >> score [

	^score
]

{ #category : #accessing }
Robot >> score: anObject [

	score := anObject
]

{ #category : #running }
Robot >> setup [

	self isSetup ifTrue: [^self].
	strategy isNil ifTrue: [strategy := self strategyClass new].
	self strategy setupOn: self.
	isSetup := true
]

{ #category : #running }
Robot >> step [

	self isDead ifTrue: [
		direction := nil.
		^self].
	strategy stepRobot: self.
]

{ #category : #printing }
Robot >> storeOn: aStream [

	aStream
		nextPut: $(;
		nextPutAll: self class name;
		nextPutAll: ' new location: ', self location storeString;
		nextPutAll: '; yourself)'
]

{ #category : #accessing }
Robot >> strategy [

	^strategy
]

{ #category : #accessing }
Robot >> strategy: aStrategy [

	strategy := aStrategy.
]

{ #category : #accessing }
Robot >> strategyClass [

	^strategyClass
]

{ #category : #accessing }
Robot >> strategyClass: anObject [

	strategyClass := anObject
]
