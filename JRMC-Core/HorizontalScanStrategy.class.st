Class {
	#name : #HorizontalScanStrategy,
	#superclass : #Strategy,
	#instVars : [
		'stepsToNextRow'
	],
	#category : #'JRMC-Core'
}

{ #category : #identification }
HorizontalScanStrategy class >> teamName [

	^'Competition Organizer'
]

{ #category : #running }
HorizontalScanStrategy >> setupOn: aRobot [

	stepsToNextRow := aRobot board size y + 1.
]

{ #category : #running }
HorizontalScanStrategy >> stepRobot: aRobot [

	stepsToNextRow := stepsToNextRow - 1.
	stepsToNextRow = 0
		ifTrue: [
			aRobot direction: #down.
			stepsToNextRow := aRobot board size x + 1]
		ifFalse: [aRobot direction: #right]
]

{ #category : #accessing }
HorizontalScanStrategy >> stepsToNextRow [

	^stepsToNextRow
]

{ #category : #accessing }
HorizontalScanStrategy >> stepsToNextRow: anObject [

	stepsToNextRow := anObject
]
