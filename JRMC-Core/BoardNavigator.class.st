Class {
	#name : #BoardNavigator,
	#superclass : #Object,
	#instVars : [
		'board',
		'location',
		'dead',
		'action'
	],
	#category : #'JRMC-Core'
}

{ #category : #accessing }
BoardNavigator >> action [

	^action
]

{ #category : #accessing }
BoardNavigator >> action: anObject [

	action := anObject
]

{ #category : #accessing }
BoardNavigator >> board [

	^board
]

{ #category : #accessing }
BoardNavigator >> board: anObject [

	board := anObject
]

{ #category : #calculating }
BoardNavigator >> distanceTo: aBoardNavigator [

	^(aBoardNavigator location x - self location x) abs + (aBoardNavigator location y - self location y) abs
]

{ #category : #navigating }
BoardNavigator >> down [

	self moveBy: 0 @ 1
]

{ #category : #navigating }
BoardNavigator >> down: aNumber [

	aNumber timesRepeat: [self down]
]

{ #category : #testing }
BoardNavigator >> isDead [

	^dead ifNil: [false]
]

{ #category : #navigating }
BoardNavigator >> left [

	self moveBy: -1 @ 0
]

{ #category : #navigating }
BoardNavigator >> left: aNumber [

	aNumber timesRepeat: [self left]
]

{ #category : #accessing }
BoardNavigator >> location [

	^location
]

{ #category : #accessing }
BoardNavigator >> location: anObject [

	location := anObject
]

{ #category : #accessing }
BoardNavigator >> markAlive [

	dead := false
]

{ #category : #accessing }
BoardNavigator >> markDead [

	dead := true
]

{ #category : #navigating }
BoardNavigator >> moveBy: delta [

	location := board normalizePoint: location + delta.
	action := board at: location.
	(action isKindOf: Array) ifFalse: [^self].

	action first = #warp ifTrue: [^location := (action at: 2) @ (action at: 3) ].
	action first = #die ifTrue: [self markDead].
	action first = #jump ifTrue: [
		action last timesRepeat: [location := board normalizePoint: location + delta]
		]
]

{ #category : #navigating }
BoardNavigator >> right [

	self moveBy: 1 @ 0
]

{ #category : #navigating }
BoardNavigator >> right: aNumber [

	aNumber timesRepeat: [self right]
]

{ #category : #navigating }
BoardNavigator >> up [

	self moveBy: 0 @ -1
]

{ #category : #navigating }
BoardNavigator >> up: aNumber [

	aNumber timesRepeat: [self up]
]

{ #category : #navigating }
BoardNavigator >> value [

	^board at: location
]
