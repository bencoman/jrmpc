Class {
	#name : #ImprovedRandomWalkStrategy,
	#superclass : #Strategy,
	#instVars : [
		'random',
		'lastDirection'
	],
	#category : #'JRMC-Core'
}

{ #category : #identification }
ImprovedRandomWalkStrategy class >> teamName [

	^'Competition Organizer'
]

{ #category : #'initialize-release' }
ImprovedRandomWalkStrategy >> initialize [

	random := Random new
]

{ #category : #accessing }
ImprovedRandomWalkStrategy >> random [

	^random
]

{ #category : #accessing }
ImprovedRandomWalkStrategy >> random: anObject [

	random := anObject
]

{ #category : #running }
ImprovedRandomWalkStrategy >> stepRobot: aRobot [

	| directions |
	directions := #(#left #right #up #down) copyWithout: lastDirection.

	aRobot direction: (directions at: (random next * directions size) truncated + 1).
	aRobot direction = #left ifTrue: [lastDirection := #right].
	aRobot direction = #right ifTrue: [lastDirection := #left].
	aRobot direction = #up ifTrue: [lastDirection := #down].
	aRobot direction = #down ifTrue: [lastDirection := #up].
]
