Class {
	#name : #BoardView,
	#superclass : #'VW_UI_SimpleView',
	#instVars : [
		'hoverValue',
		'hoverLocation'
	],
	#category : #'JRMC-porting-UI'
}

{ #category : #'private-events' }
BoardView class >> constructEventsTriggered [

	^super constructEventsTriggered
		add: #changedHoverValue;
		yourself
]

{ #category : #displaying }
BoardView >> boardInset [

	^20 @ 20
]

{ #category : #displaying }
BoardView >> boxForCellAt: aPoint [

	| innerBounds box |
	innerBounds := self bounds insetBy: self boardInset.
	box := ((aPoint x - 1) * innerBounds width // model size x)
				@ ((aPoint y - 1) * innerBounds height // model size y)
					+ innerBounds origin
					corner: (aPoint x * innerBounds width // model size x)
							@ (aPoint y * innerBounds height // model size y) + innerBounds origin.
	^box
]

{ #category : #displaying }
BoardView >> colorOfCell: point value: value [

	(model hasRobotAt: point) ifTrue: [^ColorValue green].
	value isInteger ifFalse: [^ColorValue black].

	value > 100.0 ifTrue: [^ColorValue yellow].

	value < -100.0 ifTrue: [^ColorValue blue].
	value < 0 ifTrue: [^ColorValue red: 1.0  - (value negated / 100.0) green: 1.0  - (value negated / 100.0) blue: 1.0].
	^ColorValue red: 1.0 green: 1.0  - (value / 100.0) blue: 1.0  - (value / 100.0)
]

{ #category : #'controller accessing' }
BoardView >> defaultControllerClass [

	^BoardController
]

{ #category : #displaying }
BoardView >> displayCellOn: aGraphicsContext at: aPoint color: aColor [

	| box value |
	box := self boxForCellAt: aPoint.
	aGraphicsContext
		paint: aColor;
		displayRectangle: box;
		paint: ColorValue black;
		displayRectangularBorder: box.
	
	(model hasRobotAt: aPoint)
		ifTrue: 
			[aGraphicsContext
				paint: ColorValue black;
				displayDotOfDiameter: box width * 0.6 at: box center].

	((value := model at: aPoint) isKindOf: Array) ifTrue: [
		value first = #die ifTrue: [self drawDieOn: aGraphicsContext in: box].
		value first = #warp ifTrue: [self drawWarpOn: aGraphicsContext in: box].
		value first = #jump ifTrue: [self drawJumpOn: aGraphicsContext in: box]]
]

{ #category : #displaying }
BoardView >> displayOn: aGraphicsContext [

	model isNil ifTrue: [^self].

	model cellLocationsDo: [:point :value |
		self displayCellOn: aGraphicsContext copy at: point color: (self colorOfCell: point value: value)]
]

{ #category : #displaying }
BoardView >> drawDieOn: aGraphicsContext in: box [
	
	| insideBox |
	insideBox := box insetBy: (box width / 6).
	aGraphicsContext
		paint: ColorValue white;
		lineWidth: 2;
		displayLineFrom: insideBox topLeft to: insideBox bottomRight;
		displayLineFrom: insideBox topRight to: insideBox bottomLeft
]

{ #category : #displaying }
BoardView >> drawJumpOn: aGraphicsContext in: box [
	
	| insideBox |
	insideBox := box insetBy: (box width / 6).
	aGraphicsContext copy
		paint: ColorValue white;
		lineWidth: 2;
		displayPolyline: (Array
			with: insideBox topCenter
			with: insideBox bottomLeft
			with: insideBox bottomRight
			with: insideBox topCenter)
]

{ #category : #displaying }
BoardView >> drawWarpOn: aGraphicsContext in: box [
	
	| insideBox |
	insideBox := box insetBy: (box width / 6).
	aGraphicsContext copy
		paint: ColorValue white;
		lineWidth: 2;
		displayRectangularBorder: insideBox
]

{ #category : #hovering }
BoardView >> hoverAtLocation: aPoint [

	| newHoverValue newHoverLocation |
	self model isNil ifTrue: [^self].

	newHoverLocation := self hoverLocationFor: aPoint.
	newHoverLocation isNil ifTrue: [
		self triggerEvent: #changedHoverValue.
		^hoverValue := nil].
	
	newHoverValue := self model at: newHoverLocation.
	(newHoverValue = hoverValue and: [newHoverLocation = hoverLocation]) ifTrue: [^self].
	hoverValue := newHoverValue.
	hoverLocation := newHoverLocation.
	self triggerEvent: #changedHoverValue
]

{ #category : #accessing }
BoardView >> hoverLocation [

	^hoverLocation
]

{ #category : #accessing }
BoardView >> hoverLocation: anObject [

	^hoverLocation := anObject
]

{ #category : #hovering }
BoardView >> hoverLocationFor: aPoint [

	| innerBounds |
	innerBounds := self bounds insetBy: self boardInset.
	(innerBounds containsPoint: aPoint) ifFalse: [^nil].

	^(((aPoint - innerBounds origin) / (innerBounds width asFloat @ innerBounds height asFloat)) * self model size) truncated + (1 @ 1).
]

{ #category : #accessing }
BoardView >> hoverValue [

	^hoverValue
]

{ #category : #'initialize-release' }
BoardView >> initialize [

	super initialize.
	hoverValue := 0
]
