Class {
	#name : #BoardEditorView,
	#superclass : #BoardView,
	#instVars : [
		'paintValueHolder',
		'line',
		'value'
	],
	#category : #'JRMC-porting-UI'
}

{ #category : #'controller accessing' }
BoardEditorView >> defaultControllerClass [

	^BoardEditorController
]

{ #category : #displaying }
BoardEditorView >> displayOn: aGraphicsContext [

	super displayOn: aGraphicsContext.
	line ifNotNil: [
		aGraphicsContext paint: ColorValue black.
		line displayStrokedOn: aGraphicsContext]
]

{ #category : #accessing }
BoardEditorView >> line [

	^line
]

{ #category : #accessing }
BoardEditorView >> line: anObject [

	line := anObject
]

{ #category : #accessing }
BoardEditorView >> paintValueHolder [

	^paintValueHolder
]

{ #category : #accessing }
BoardEditorView >> paintValueHolder: anObject [

	paintValueHolder := anObject
]

{ #category : #accessing }
BoardEditorView >> value [

	^value
]

{ #category : #accessing }
BoardEditorView >> value: anObject [

	value := anObject
]
