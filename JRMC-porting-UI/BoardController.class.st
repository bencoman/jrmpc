Class {
	#name : #BoardController,
	#superclass : #'VW_UI_Controller',
	#category : #'JRMC-porting-UI'
}

{ #category : #events }
BoardController >> mouseMovedEvent: event [

	view hoverAtLocation: (view globalPointToLocal: event point).
]
