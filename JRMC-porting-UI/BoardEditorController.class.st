Class {
	#name : #BoardEditorController,
	#superclass : #BoardController,
	#instVars : [
		'trackerClass'
	],
	#category : #'JRMC-porting-UI'
}

{ #category : #events }
BoardEditorController >> redButtonPressedEvent: event [

	trackerClass ifNil: [^self].
	self selectEvent: event
]

{ #category : #'event driven' }
BoardEditorController >> selectionTracker [

	^trackerClass on: self
]

{ #category : #accessing }
BoardEditorController >> trackerClass [

	^trackerClass
]

{ #category : #accessing }
BoardEditorController >> trackerClass: anObject [

	trackerClass := anObject
]
