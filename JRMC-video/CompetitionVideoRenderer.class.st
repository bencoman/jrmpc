Class {
	#name : #CompetitionVideoRenderer,
	#superclass : #Object,
	#instVars : [
		'window',
		'isRunning',
		'resizeJob',
		'renderJob',
		'idleJob',
		'context'
	],
	#category : #'JRMC-video'
}

{ #category : #private }
CompetitionVideoRenderer class >> defaultWindowSize [
	^1280 @ 960
]

{ #category : #opening }
CompetitionVideoRenderer class >> open [
	^self openInExtent: self defaultWindowSize
]

{ #category : #opening }
CompetitionVideoRenderer class >> openInExtent: anExtent [
	^self new openInExtent: anExtent
]

{ #category : #opening }
CompetitionVideoRenderer >> idle: job [
	" The standard idle function. If we're in vsync mode when we start up, there's no need to idle. Otherwise, we'll idle at 60hz. "

	(context whileCurrentDo: [context isVSyncEnabled]) ifTrue: [^self].
	[isRunning] whileTrue:
		[
		Transcript show: 'Tick'; cr.
		(Delay forMilliseconds: 100.0 - job disabled) wait.
		job pause]
]

{ #category : #opening }
CompetitionVideoRenderer >> initializeJobs [
	renderJob := Animation named: 'render' do: [:job | self render: job].
	idleJob := Animation named: 'idle' do: [:job | self idle: job]
]

{ #category : #opening }
CompetitionVideoRenderer >> initializeOpenGL [
	context := RenderContext on: window.
	context whileCurrentDo:
		[self initializeSettings.
		self initializeViewport]
]

{ #category : #opening }
CompetitionVideoRenderer >> initializeSettings [

	" Try and default to vsync enabled, for the smoothest rendering. "
	context isVSyncEnabled: true.

	" Set our default background color to black. "
	gl ClearColor: 0 with: 0 with: 0 with: 1.

	" Set our default depth buffer value to 1 "
	gl ClearDepth: 1.

	" Enable depth testing and culling by default. "
	gl Enable: gl DEPTH_TEST.
	gl DepthFunc: gl LEQUAL.
	gl Enable: gl CULL_FACE.
]

{ #category : #opening }
CompetitionVideoRenderer >> initializeViewport [
	gl Viewport: window bounds left with: window bounds top with: window bounds width with: window bounds height
]

{ #category : #opening }
CompetitionVideoRenderer >> initializeWindowEvents [
	window when: #closing send: #windowClosing to: self.
	window when: #bounds send: #windowResize to: self.
	window keyboardProcessor keyboardHook: [:event :controller | self processKeyboardEvent: event. event].
]

{ #category : #opening }
CompetitionVideoRenderer >> main [
	" The main method - subclasses can override and replace this method if their frame workflow is different. Our workflow is simple, resize the window if we have to, render, idle if we're not vsyncing. "

	resizeJob ifNotNil: [resizeJob resume].
	renderJob resume.
	idleJob resume
]

{ #category : #opening }
CompetitionVideoRenderer >> open [

	self openInExtent: self class defaultWindowSize;
	start
]

{ #category : #opening }
CompetitionVideoRenderer >> openInExtent: anExtent [
	self windowInExtent: anExtent.
	window open.
	self start
]

{ #category : #opening }
CompetitionVideoRenderer >> processKeyboardEvent: anEvent [
	anEvent keyValue = Character esc ifTrue: [self quit]
]

{ #category : #opening }
CompetitionVideoRenderer >> quit [
	" We've been told to quit, close the window, this will fire the #windowClosing event which will stop the main loop. "

	window controller closeAndUnschedule
]

{ #category : #opening }
CompetitionVideoRenderer >> releaseOpenGL [
	context release
]

{ #category : #opening }
CompetitionVideoRenderer >> releaseWindowEvents [
	window keyboardProcessor keyboardHook: nil.
	window removeAllActionsWithReceiver: self
]

{ #category : #opening }
CompetitionVideoRenderer >> render [
	" The main rendering method - subclasses will override this method to change what is rendered to the screen. "

	gl Clear: (gl COLOR_BUFFER_BIT bitOr: gl DEPTH_BUFFER_BIT)
]

{ #category : #opening }
CompetitionVideoRenderer >> render: job [
	" The standard rendering loop. Stop rendering when we're not longer running. "

	[isRunning] whileTrue:
		[context whileCurrentDo:
			[self render.
			context flush].
		job pause]
]

{ #category : #opening }
CompetitionVideoRenderer >> start [
	" The standard start/stop functionality for the main loop. "

	| process |
	isRunning := true.
	process :=
		[self starting.
		[[isRunning] whileTrue: [self main].
		self main]
			ensure: [self stopping]] newProcess.
	process name: (Processor activeProcess name ifNil: ['']), ' - main loop'.
	process resume
]

{ #category : #opening }
CompetitionVideoRenderer >> starting [
	self initializeWindowEvents.
	self initializeOpenGL
	self initializeJobs
]

{ #category : #opening }
CompetitionVideoRenderer >> stop [
	" The standard start/stop functionality for the main loop. "

	isRunning := false
]

{ #category : #opening }
CompetitionVideoRenderer >> stopping [
	self releaseOpenGL
	self releaseWindowEvents
]

{ #category : #opening }
CompetitionVideoRenderer >> windowClosing [
	self stop
]

{ #category : #opening }
CompetitionVideoRenderer >> windowInExtent: anExtent [
	window := ApplicationWindow new.
	window model: self.
	window minimumSize: anExtent.
	window keyboardProcessor: KeyboardProcessor new
]

{ #category : #opening }
CompetitionVideoRenderer >> windowResize [
	" We've been told our windoer has resized, schedule a resize job for the main loop. "

	resizeJob := Animation named: 'resize' do: [:job |
		context resize.
		context whileCurrentDo: [self initializeViewport].
		resizeJob := nil]
]
