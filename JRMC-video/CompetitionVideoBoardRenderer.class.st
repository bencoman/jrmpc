Class {
	#name : #CompetitionVideoBoardRenderer,
	#superclass : #CompetitionVideoRenderer,
	#instVars : [
		'board',
		'geometry',
		'geometryVectors',
		'program',
		'boardVectors',
		'modelViewMatrix',
		'projectionMatrix',
		'geometryColors',
		'refreshRequired',
		'runCount',
		'textProgram',
		'font',
		'text',
		'updateJob',
		'textCamera',
		'textBuffer',
		'textAttributeArray',
		'textToDisplay'
	],
	#category : #'JRMC-video'
}

{ #category : #adding }
CompetitionVideoBoardRenderer >> addText: aText as: aSymbol [

	textToDisplay at: aSymbol put: aText
]

{ #category : #accessing }
CompetitionVideoBoardRenderer >> board [

	^board
]

{ #category : #accessing }
CompetitionVideoBoardRenderer >> board: anObject [

	board := anObject
]

{ #category : #accessing }
CompetitionVideoBoardRenderer >> boardVectors [

	^boardVectors
		ifNil: [| vectorStream array |
			vectorStream := WriteStream on: (Array new: 10000).
			array := self writeBoardVectorsOn: vectorStream.
			boardVectors := FloatArray withAll: array]
		ifNotNil: [
			| vectorStream |
			vectorStream := WriteStream on: boardVectors.
			self writeBoardVectorsOn: vectorStream]
]

{ #category : #displaying }
CompetitionVideoBoardRenderer >> colorOfCell: point value: value [

	(board hasRobotAt: point) ifTrue: [^#(0.0 1.0 0.0 1.0)].
	value isInteger ifFalse: [^#(0.0 0.0 0.0 1.0)].

	value > 1000.0 ifTrue: [^#(1.0 0.4 0.6 1.0)].
	value > 100.0 ifTrue: [^#(1.0 1.0 0.0 1.0)].

	value < -100.0 ifTrue: [^#(0.0 0.0 1.0 1.0)].
	value < 0 ifTrue: [^Array with: 1.0  - (value negated / 100.0) with: 1.0  - (value negated / 100.0) with: 1.0 with: 1.0].
	^Array with: 1.0 with: 1.0  - (value / 100.0) with: 1.0  - (value / 100.0) with: 1.0
]

{ #category : #shaders }
CompetitionVideoBoardRenderer >> fragmentShader [
^'
varying vec4 theColor;
void main()
{
	gl_FragColor = theColor;
}'
]

{ #category : #shaders }
CompetitionVideoBoardRenderer >> fragmentShaderMain [
^'
void applyTextureCoordinate();

void main()
{
	applyTextureCoordinate();
}'
]

{ #category : #shaders }
CompetitionVideoBoardRenderer >> fragmentShaderTextureCoordinate [
^'
uniform sampler2D texture;
varying vec2 fragmentTextureCoordinate;

void applyTextureCoordinate()
{
	gl_FragColor = texture2D(texture,fragmentTextureCoordinate);
}'
]

{ #category : #'initialize-release' }
CompetitionVideoBoardRenderer >> initializeFont [

	" Build our font object. "
	font := (VW_OpenGLFont fromFontDescription: (FontDescription new
		family: 'Helvetica';
		pixelSize: 24;
		boldness: 0.6;
		italic: false))
		build.
]

{ #category : #'initialize-release' }
CompetitionVideoBoardRenderer >> initializeGeometry [
	| data |
	data := self boardVectors.

	geometry isNil ifTrue: [geometry := Buffer newArrayBuffer].
	geometry data: data mode: gl STATIC_DRAW.

	geometryVectors := geometry asAttributeArray: OpenGLVector4 stride: 8 offset: 0.
	geometryColors := geometry asAttributeArray: OpenGLColor4 stride: 8 offset: 4
]

{ #category : #'initialize-release' }
CompetitionVideoBoardRenderer >> initializeJobs [

	super initializeJobs.
	updateJob := Animation named: 'update' do: [:job | self updateText: job]
]

{ #category : #'initialize-release' }
CompetitionVideoBoardRenderer >> initializeMatrices [

	modelViewMatrix := OpenGLMatrix4 translationX: 0 y: 0 z: -2.
	modelViewMatrix := modelViewMatrix * (OpenGLMatrix4 rotationX: -45.0 * Float pi / 180.0).
	modelViewMatrix := modelViewMatrix * (OpenGLMatrix4 rotationZ: -75.0 * Float pi / 180.0).

	projectionMatrix := OpenGLMatrix4 perspective: 45 degreesToRadians aspect: window bounds width / window bounds height near: 1 far: 100
]

{ #category : #'initialize-release' }
CompetitionVideoBoardRenderer >> initializeProgram [

	program := Program new.
	program attach: (Shader newVertexShader: self vertexShader).
	program attach: (Shader newFragmentShader: self fragmentShader).
	program link
]

{ #category : #'initialize-release' }
CompetitionVideoBoardRenderer >> initializeSettings [

	refreshRequired := true.
	runCount := 60.
	textToDisplay := Dictionary new.

	super initializeSettings.
	self initializeProgram.
	self initializeGeometry.

	self initializeMatrices.

	self initializeTextProgram.
	self initializeFont.

	gl Enable: gl BLEND.
	gl BlendFunc: gl ONE_MINUS_DST_COLOR with: gl ZERO
]

{ #category : #'initialize-release' }
CompetitionVideoBoardRenderer >> initializeTextProgram [

	textCamera := Matrix4 orthogonal: window bounds near: -1 far: 0.

	textProgram := Program new
		attach: (Shader newVertexShader: self vertexShaderTextureCoordinate);
		attach: (Shader newVertexShader: self vertexShaderPosition);
		attach: (Shader newVertexShader: self vertexShaderMain);
		attach: (Shader newFragmentShader: self fragmentShaderTextureCoordinate);
		attach: (Shader newFragmentShader: self fragmentShaderMain);
		link.
]

{ #category : #opening }
CompetitionVideoBoardRenderer >> main [

	updateJob resume.
	super main
]

{ #category : #opening }
CompetitionVideoBoardRenderer >> processKeyboardEvent: anEvent [

	Transcript show: anEvent keyValue printString; cr.
	anEvent keyValue = #Left ifTrue: [
		board robot isSetup ifFalse: [board robot setup].
		board step: 0.1s.
		board moveRobot.
		refreshRequired := true]
]

{ #category : #displaying }
CompetitionVideoBoardRenderer >> render [
	" Render our scene to the window. "
	super render.

	" Tell the program we want to use it. This statement actives this particular program, but there's no reason why we can't have multiple programs. "
	program use.

	program uniform: 'projectionMatrix' value: projectionMatrix.
	program uniform: 'modelViewMatrix' value: modelViewMatrix.

	program attribute: 'myVertex' array: geometryVectors.

	" Bind the 'myVertex' variable in our vertex shader program to the data in our triangle geometry buffer. As the vertex program runs, it will read data from our buffer. "
	geometry bind.
	program attribute: 'myVertex' components: 4 type: gl FLOAT normalized: false stride: 0 offset: 0.

	refreshRequired ifTrue: [self initializeGeometry. refreshRequired := false].
	" The geometryVectors variable we set up in #initializeGeometry knows how we've laid out the data inside our buffer, so we can use it to set up our attribute instead, which is easier. "
	program attribute: 'myVertex' array: geometryVectors.
	program attribute: 'myColor' array: geometryColors.

	" Execute our program for all our components in our buffer. "
	gl DrawArrays: gl TRIANGLES with: 0 with: self boardVectors size // 4.

	" Stop using the program. "
	program unuse.

	textProgram use.
	textProgram uniform: 'camera' value: textCamera.
	textToDisplay do: [:each | each render].
	textProgram unuse
]

{ #category : #displaying }
CompetitionVideoBoardRenderer >> render: job [
	" The standard rendering loop. Stop rendering when we're not longer running. "

	[isRunning] whileTrue: [
		runCount := runCount - 1.
		(runCount <= 0 and: [board isFinished not]) ifTrue: [
			runCount := 6.
			board robot isSetup ifFalse: [board robot setup].
			board step: 0.1s.
			board moveRobot.
			refreshRequired := true].
				
		context whileCurrentDo:
			[self render.
			context flush].
	
		job pause]
]

{ #category : #opening }
CompetitionVideoBoardRenderer >> scoreTextFor: aNumber [

	| printConverter |
	printConverter := VW_PrintConverter for: #number withFormatString: '#,##0'.
	^(printConverter formatStringFor: aNumber) asString, '    ', (board time printString copyWithout: $s), ' of ', (board endTime printString copyWithout: $s)
]

{ #category : #opening }
CompetitionVideoBoardRenderer >> starting [

	super starting.
	board robot setup
]

{ #category : #opening }
CompetitionVideoBoardRenderer >> updateText: job [

	" We're going to create an OpenGLText object every second with the latest time. Calling #text: on the Font gives us a partially completed RenderState, we just need to fill in our program and any extra variables that program is expecting - in our case, the camera. "

	| oldScore |
	oldScore := board robot s
	context whileCurrentDo: [
		self
			addText: (font text: (board robot strategy class teamName) position: 600 @ 0) as: #teamName;
			addText: (font text: (self scoreTextFor: oldScore) position: 600 @ 30) as: #score].

	[isRunning] whileTrue:
		[
			context whileCurrentDo: [
			(board time > 0 and: [board time \\ 5 = 0]) ifTrue: [
				self addText: (font text: (self scoreTextFor: board robot score) position:  600 @ 60) as: #fiveSecondScore].
			
			board robot score = oldScore ifFalse:
				[
				oldScore := board robot s
				self addText: (font text: (self scoreTextFor: oldScore) position:  600 @ 30) as: #score]].
		job pause]
]

{ #category : #shaders }
CompetitionVideoBoardRenderer >> vertexShader [
^'
/* We define two new uniforms and get rid of the last two uniforms. These two uniforms are 4x4 matrices.
    The modelViewMatrix takes the place of the rotate and shear operations we did before.
    The projectionMatrix is new and takes on the role of transforming the scene to be looked at from a "camera". */
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

attribute vec4 myVertex;
attribute vec4 myColor;
varying vec4 theColor;
void main()
{
	/* In this shader, we recreate what OpenGL used to do, by first transforming the scene by the modelViewMatrix, which generally involves translations and rotations */
	vec4 eyeCoordinate = modelViewMatrix * myVertex;

	/* The next coordinate conversion is to apply the projectionMatrix to the vertex so that it moves to its correct camera position */
	vec4 clipCoordinate = projectionMatrix * eyeCoordinate;

	/* The last two coordinate transformations are performed by the graphics card, but I include them them below for you to see anyway:
	vec3 deviceCoordinate = (clipCoordinate / clipCoordinate w) xyz;
	vec3 windowCoordinate =
		viewport width half * deviceCoordinate x + viewport width half,
		viewport height half * deviceCoordinate y + viewport height half,
		(far - near) half * deviceCoordinate z + (near + far) half
	*/

	gl_Position = clipCoordinate;
	theColor = myColor;
}'
]

{ #category : #shaders }
CompetitionVideoBoardRenderer >> vertexShaderMain [
^'
void applyTextureCoordinate();
void applyPosition();

void main()
{
	applyTextureCoordinate();
	applyPosition();
}'
]

{ #category : #shaders }
CompetitionVideoBoardRenderer >> vertexShaderPosition [
^'
uniform mat4 camera;
attribute vec4 position;

void applyPosition()
{
	gl_Position = camera * position;
}'
]

{ #category : #shaders }
CompetitionVideoBoardRenderer >> vertexShaderTextureCoordinate [
^'
attribute vec2 textureCoordinate;
varying vec2 fragmentTextureCoordinate;

void applyTextureCoordinate()
{
	fragmentTextureCoordinate = textureCoordinate;
}'
]

{ #category : #accessing }
CompetitionVideoBoardRenderer >> writeBoardVectorsOn: vectorStream [

		| width |
		1 to: board size x do: [:x |
			1 to: board size y do: [:y |
				| xCoord yCoord xWidth yWidth |
				xCoord := (x asFloat / board size x asFloat) - 0.5.
				yCoord := (y asFloat / board size y asFloat) - 0.5.
				xWidth := 1.0 / board size x asFloat.
				yWidth := 1.0 / board size y asFloat.
		
				vectorStream nextPut: xCoord - xWidth; nextPut: yCoord - yWidth; nextPut: -0.001; nextPut: 1.0; nextPutAll: (self colorOfCell: y @ x value: (board at: y @ x)).
				vectorStream nextPut: xCoord; nextPut: yCoord - yWidth; nextPut: -0.001; nextPut: 1.0; nextPutAll: (self colorOfCell: y @ x value: (board at: y @ x)).
				vectorStream nextPut: xCoord; nextPut: yCoord; nextPut: -0.001; nextPut: 1.0; nextPutAll: (self colorOfCell: y @ x value: (board at: y @ x)).
			
				vectorStream nextPut: xCoord - xWidth; nextPut: yCoord - yWidth; nextPut: -0.001; nextPut: 1.0; nextPutAll: (self colorOfCell: y @ x value: (board at: y @ x)).
				vectorStream nextPut: xCoord; nextPut: yCoord; nextPut: -0.001; nextPut: 1.0; nextPutAll: (self colorOfCell: y @ x value: (board at: y @ x)).
				vectorStream nextPut: xCoord - xWidth; nextPut: yCoord; nextPut: -0.001; nextPut: 1.0; nextPutAll: (self colorOfCell: y @ x value: (board at: y @ x)).
				]
			].

		0 to: board size x do: [:x |
			| xCoord |
			xCoord := (x asFloat / board size x asFloat) - 0.5.
			width := 1.0 / 1000.0.
			
			vectorStream nextPut: xCoord - width; nextPut: -0.5; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).
			vectorStream nextPut: xCoord + width; nextPut: -0.5; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).
			vectorStream nextPut: xCoord + width; nextPut: 0.5; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).
			
			vectorStream nextPut: xCoord - width; nextPut: -0.5; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).
			vectorStream nextPut: xCoord + width; nextPut: 0.5; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).
			vectorStream nextPut: xCoord - width; nextPut: 0.5; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).
			].

		0 to: board size y do: [:y |
			| yCoord |
			yCoord := (y asFloat / board size y asFloat) - 0.5.
			width := 1.0 / 1000.0.
			
			vectorStream nextPut: -0.5; nextPut: yCoord - width; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).
			vectorStream nextPut: 0.5; nextPut: yCoord - width; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).
			vectorStream nextPut: 0.5; nextPut: yCoord + width; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).

			vectorStream nextPut: -0.5; nextPut: yCoord - width; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).
			vectorStream nextPut: 0.5; nextPut: yCoord + width; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).
			vectorStream nextPut: -0.5; nextPut: yCoord + width; nextPut: 0.0; nextPut: 1.0; nextPutAll: #( 0.4 0.4 0.4 1.0 ).
			].

		^vectorStream contents
]
